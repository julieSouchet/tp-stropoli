# StroPoli

## Site de création de sondages

![page d'accueil](./models/accueil.png)

StroPoli permet à l'utilisateur de créer des sondages, ainsi que de voter sur des sondages déjà existants. Voter nécessite forcément un compte d'utilisateur pour éviter qu'une même personne vote plusieurs fois.

On peut voir la liste :

- de tous les sondages du site (URL : "/sondage")
  ![liste des sondages](./models/liste-sondages.png)

- ceux créés par un seul utilisateur (URL : "/profile/{id}")
  ![sondages d'un autre utilisateur](./models/user-sondages.png)
  
  - après s'être connecté, ceux qu'on a créés nous-mêmes (URL: "/profile")
    ![sondages de l'utilisateur connecté](./models/mes-sondages.png)

- rechercher des sondages par leurs questions, avec le moteur de recherche (URL : "/search?query={question}")
  ![résultats de recherche](./models/recherche-sondages.png)

On a évidemment la possibilité de voir les résultats des votes (URL : "/sondage/{id}")
![résultats des votes](./models/resultats.png)

Un utilisateur connecté peut également voter (URL : "/sondage/{id}/vote")...
![voter](./models/voter.png)

... et voir quels votes il a choisi par le passé (URL : "profile/votes")

![votes de l'utilisateur connecté](./models/mes-votes.png)

### Installer le projet pour développement :

- Clôner le dépôt Git :

`git clone https://gitlab.com/julieSouchet/tp-stropoli.git`

`cd tp-stropoli`

- Configurer la base de données dans un fichier `.env.local` situé à la racine du projet. Celui-ci doit contenir :

```
APP_ENV=dev
APP_SECRET=5de6d22033560d5893b3fc7eb7df0eae
DATABASE_URL=mysql://db_user:db_password@address:port/db_name?serverVersion=server_version
```

Avec `mysql` = le serveur SQL utilisé

`db_user` = votre nom d'utilisateur pour SQL

`db_password` = votre mot de passe

`db_name` = le nom de la BDD du projet

`address:port` = l'adresse et le port du serveur SQL

`server_version`= la version du serveur SQL

- Puis installer les dépendances avec :

`npm install`

`composer install`

- Compiler automatiquement le code source :

`npm run watch`

- Et pour finir, lancer le serveur de développement de Symfony :

`symfony serve`

Vous êtes prêts !



### Technos utilisées :

- [HTML5](https://developer.mozilla.org/fr/docs/Web/HTML), [CSS3](https://developer.mozilla.org/fr/docs/Web/CSS)

- [Bootstrap](https://getbootstrap.com/)

- [Sass](https://sass-lang.com/)

- [TypeScript](https://www.typescriptlang.org/)

- [PHP7](https://www.php.net/)

- [Symfony](https://symfony.com/)

- [BreadcrumbsBundle de mhujer](https://github.com/mhujer/BreadcrumbsBundle)

- [Webpack-Encore](https://symfony.com/doc/current/frontend.html)

- [MariaDB10](https://mariadb.org/)
