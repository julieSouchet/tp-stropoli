document.addEventListener('DOMContentLoaded', () => {
    const reponsesList = document.getElementById('reponses-list') as HTMLUListElement;
    const reponsePrototype : string = reponsesList.dataset.prototype;

    // 2 réponses minimum obligatoires
    document.getElementById('sondage_reponses_0_texte').setAttribute('required', 'true');
    document.getElementById('sondage_reponses_1_texte').setAttribute('required', 'true');

    // ajout de nouvelles réponses
    let reponseNumber : number = parseInt(reponsesList.dataset.reponseNumber, 10);
    let lastReponse = document.querySelectorAll('#reponses-list .reponse-input')[reponseNumber-1] as HTMLInputElement;

    const addReponse = () => {
        console.log(reponseNumber + ' input ' + lastReponse.id);
        reponsesList.innerHTML += reponsePrototype
            .replace(/__name__/g, reponseNumber.toString())
            .replace(/__label__/, `Réponse ${(reponseNumber + 1).toString()} :`);
        reponseNumber++;
        lastReponse.removeEventListener('input', addReponse);
        lastReponse = document.querySelectorAll('#reponses-list .reponse-input')[reponseNumber-1] as HTMLInputElement;
        lastReponse.addEventListener('input', addReponse)
    };

    lastReponse.addEventListener('input', addReponse)
});