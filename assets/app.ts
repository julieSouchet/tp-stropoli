/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// images
// import './images/favicon.ico';
// import './images/tophat_with_pages.svg';
// import './images/background.png';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
// bootstrap
import 'bootstrap/dist/js/bootstrap.bundle.min';

document.addEventListener('DOMContentLoaded', () => {
    // links for copying the current URL to clipboard
    const copyButtons = document.getElementsByClassName('copy-link') as HTMLCollectionOf<HTMLButtonElement>;
    for (let i = 0; i < copyButtons.length; i++) {
        const el = copyButtons[i];
        el.addEventListener('click', () => {
            const link = document.getElementById(el.dataset.link) as HTMLInputElement;
            link.select();
            link.setSelectionRange(0, link.value.length);
            document.execCommand("copy");
            $(el).tooltip('dispose');
            el.setAttribute('title', el.dataset.done);
            $(el).tooltip('enable');
            $(el).tooltip('show');
        });

        el.addEventListener('mouseleave', () => {
            $(el).tooltip('dispose');
            el.setAttribute('title', el.dataset.verbose);
            $(el).tooltip('enable');
        });
    }

    // tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    })

    // navbar
    const navLinks = document.getElementsByClassName('nav-link') as HTMLCollectionOf<HTMLLinkElement>;
    for (let i = 0; i < navLinks.length; i++) {
        const navLink = navLinks[i];
        if (navLink.href == window.location.href) {
            navLink.classList.add('active');
            break;
        }
    }
});