document.addEventListener('DOMContentLoaded', () => {
    const reponseItem = document.querySelectorAll('.reponse') as NodeListOf<HTMLUListElement>;
    for (let i = 0; i < reponseItem.length; i++) {
        const child = document.querySelector(`#${reponseItem[i].id} *[data-id]`) as HTMLElement;
        const id = parseInt(child.dataset.id);
        const color = calcColor(id);

        // full bg ?
        if (reponseItem[i].classList.contains('selected')) {
            reponseItem[i].style.backgroundColor = '#'+color;
            // text
            if (calcBrightness(color) < 128) {
                reponseItem[i].style.color = 'white';
            }
        } else {
            reponseItem[i].style.backgroundImage = `linear-gradient(to right, #${color} 4%, transparent 4%, transparent 96%, #${color} 96%)`;
        }
        // border
        reponseItem[i].style.borderColor = '#'+color;

        // chart
        const progressBar = document.getElementById('barre-'+id.toString(10));
        if (progressBar) {
            progressBar.style.backgroundColor = '#' + color;
            // text
            if (calcBrightness(color) > 128) {
                progressBar.style.color = 'black';
            }
        }

    }
});

/**
 * Takes a number and returns a hexadecimal color based on it.
 * @param number - hexadecimal color string value
 */
export const calcColor = (number : number) : string => {
    let color =  Math.round((number*0x123456)%0xFFFFFF).toString(16);
    if (color.length < 6 ) {
        // make sure it is the necessary length for html
        const colorArray = color.split('');
        while (colorArray.length < 6) {
            colorArray.push('0');
        }
        color = colorArray.join('');
    }
    return color;
};

/**
 * Takes a color in the hexadecimal format, and returns the same color in the rgb format (array of numbers).
 * @param hexColor - color string value (0xXXXXXX, or XXXXXX)
 */
export const rgbColor = (hexColor : string) : number[] => {
    hexColor = hexColor.replace('0x', '');
    return [parseInt('0x'+hexColor.slice(0,2)), parseInt('0x'+hexColor.slice(2,4)), parseInt('0x'+hexColor.slice(4,6))];
}

export const calcBrightness = (rgb : string) : number => {
    rgb = rgb.replace(/#|0x/g, '');
    const R : number = parseInt('0x'+rgb.slice(0,2));
    const G : number = parseInt('0x'+rgb.slice(2,4));
    const B : number = parseInt('0x'+rgb.slice(4,6));
    const lum : number = Math.round((R+R+B+G+G+G)/6);
    console.log(`0x${rgb}, R: ${R}, G: ${G}, B: ${B}, luminosité : ${lum}, ${lum < 128 ? 'foncé' : 'clair'}`);
    return lum;
};