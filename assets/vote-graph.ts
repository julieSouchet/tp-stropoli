import {Chart} from 'chart.js';
import {calcColor, rgbColor} from "./color-reponses";

document.addEventListener('DOMContentLoaded', () => {
   // read data
   const chartNode = document.getElementById('chart') as HTMLCanvasElement;
   const total = parseInt(chartNode.dataset.total, 10);
   const data : number[] = [];
   const labels : string[] = [];
   const colors: Chart.ChartColor[] = [];
   for (let i = 0; i < total; i++) {
      data.push(parseInt(chartNode.dataset['vote'+i], 10));
      labels.push(chartNode.dataset['reponse'+i]);
      colors.push('#'+calcColor(parseInt(chartNode.dataset['id'+i])));
   }
   console.table(data);
   console.table(labels);
   console.table(colors);
   // build chart
   const chart = new Chart(chartNode, {
      type: 'pie',
      data: {
         datasets: [{
            data: data,
            backgroundColor: colors,
            borderColor: 'white',
            borderAlign: 'inner',
            borderWidth: 5,
            hoverBorderColor: 'transparent'
         }],
         labels: labels
      },
      options: {
         cutoutPercentage: 33
      }
   });


   // display switch
   const bar = document.getElementById('progress-bar') as HTMLDivElement;
   document.getElementById('display-switch').addEventListener('click', () => {
      bar.classList.toggle('d-none');
      chartNode.classList.toggle('d-none');
   });
});
