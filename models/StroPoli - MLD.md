# StroPoli

## Modèle logique de données

- Sondage (<u>id</u>, question, #createur)
  
  - clé primaire : id
  
  - clé étrangère : Sondage[createur] ϵ Utilisateur[id]
  
  | Nom          | Type    | Longueur | Nullable | Clé                          |
  | ------------ | ------- | -------- | -------- | ---------------------------- |
  | id           | int     |          | non      | primaire                     |
  | question     | varchar | 512      | non      |                              |
  | dateCreateur | date    |          | non      |                              |
  | createur     | int     |          | oui      | étrangère -> Utilisateur[id] |
  
  - Utilisateur (<u>id</u>, <u>mail</u>, mdp, nom, prenom)
  
  - clé primaire : id
  
  - clé unique : mail
  
  | Nom    | Type    | Longueur | Nullable |
  | ------ | ------- | -------- | -------- |
  | id     | int     |          | non      |
  | mail   | varchar | 255      | non      |
  | mdp    | binary  | 32       | non      |
  | nom    | varchar | 255      | oui      |
  | prenom | varchar | 255      | oui      |

- Réponse (<u>id</u>, texte, #sondage)
  
  - clé primaire : id
  - clé étrangère :
    - Reponse[sondage] ϵ Sondage[id]
  
  | Nom     | Type    | Longueur | Nullable |
  | ------- | ------- | -------- | -------- |
  | id      | int     |          | non      |
  | texte   | varchar | 255      | non      |
  | sondage | int     |          | non      |

- Vote (<u>utilisateur, reponse, sondage</u>)
  
  - clé primaire : utilisateur, reponse, sondage (composée)
  - clés étrangères :
    - Vote[utilisateur] ϵ Utilisateur[id] 
    - Vote[reponse] ϵ Reponse[id]
    - Vote[sondage] ϵ Sondage[id]
  - contrainte : La date d'un vote survient forcément après la date de création du sondage correspondant !
  
  | Nom         | Type | Longueur | Nullable |
  | ----------- | ---- | -------- | -------- |
  | utilisateur | int  |          | non      |
  | reponse     | int  |          | non      |
  | sondage     | int  |          | non      |
  | date        | date |          | non      |