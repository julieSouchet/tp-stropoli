<?php

namespace App\Form;

use App\Entity\Reponse;
use App\Entity\Sondage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SondageVoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sondage = $options['data'];
        $builder
        ->add('votes', ChoiceType::class, [
            'required' => true,
            'label' => false,
            'choices' => $sondage->getReponses(),
            'choice_label' => 'texte',
            'expanded' => true,
            'multiple' => $sondage->getMultiple(),
            'mapped' => false,
            'choice_attr' => function(Reponse $reponse) {return ['data-id'=>$reponse->getId()];}
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sondage::class,
        ]);
    }
}
