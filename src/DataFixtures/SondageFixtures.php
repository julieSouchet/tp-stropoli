<?php

namespace App\DataFixtures;

use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SondageFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var int - proportion (nP / nP+1) des sondages créés ayant un créateur nommé
     * Si namedProportion == 3, alors 3/4 des créateurs seront nommés.
     * Si == 4, ce sera 4/5, etc.
     */
    private $namedProportion = 3;

    public function load(ObjectManager $manager)
    {
        $ur = $manager->getRepository(User::class);
        $faker = Factory::create();
        for ($i = 0; $i < 20; $i++) {
            $sondage = new Sondage();
            // texte aléatoire finissant par un ?
            $question = strtr($faker->text(random_int(20, 120)), ".", "?");
            $sondage->setQuestion($question);
            $sondage->setMultiple(random_int(0, 1));
            $sondage->setDateCreation($faker->dateTimeThisCentury);

            // sondages ayant un créateur nommé
            if (random_int(0, $this->namedProportion)) {
                $user = $ur->findRandom();
                $sondage->setCreateur($user);
            }

            $manager->persist($sondage);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}
