<?php

namespace App\DataFixtures;

use App\Entity\Sondage;
use App\Entity\User;
use App\Entity\Vote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class VoteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $sondageRepository = $manager->getRepository(Sondage::class);
        $userRepository = $manager->getRepository(User::class);
        $voteRepository = $manager->getRepository(Vote::class);

        for ($i = 0; $i < 100; $i++) {
            $user = $userRepository->findRandom();
            $sondage = $sondageRepository->findRandom();

            while (count($sondage->getReponses()) === 0 ||
                (!$sondage->getMultiple() && $voteRepository->hasVoted($sondage, $user))) {
                // skip Sondages w/o answers, as well as already answered, single-answer Sondage
                $sondage = $sondageRepository->findRandom();
            }

            // creating a new vote
            $vote = new Vote();
            $vote->setSondage($sondage);
            $vote->setVoteur($user);
            $dateDiff = random_int(0, 365);
            $vote->setDate($sondage->getDateCreation()->modify("+$dateDiff days"));

            // selecting random Reponse in this Sondage
            $reponses = $sondage->getReponses();
            $reponse = $reponses[random_int(0, count($reponses)-1)];
            $vote->setReponse($reponse);

            // saving the new vote in DB if not already existing in case of multiple votes
            if (!$voteRepository->exists($user, $sondage, $reponse)) {
                $manager->persist($vote);
                $manager->flush();
            }
        }

        // $product = new Product();
        // $manager->persist($product);

    }

    public function getDependencies()
    {
        return [UserFixtures::class, SondageFixtures::class, ReponseFixtures::class];
    }
}
