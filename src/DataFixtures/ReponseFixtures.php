<?php

namespace App\DataFixtures;

use App\Entity\Reponse;
use App\Entity\Sondage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReponseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $sr = $manager->getRepository(Sondage::class);
        $faker = Factory::create();
        foreach ($sr->findAll() as $sondage) {
            $nbReponses = random_int(2,6);
            for ($i = 0; $i < $nbReponses; $i++) {
                $reponse = new Reponse();
                $reponse->setTexte($faker->text(random_int(10, 50)));
                $reponse->setSondage($sondage);
                $sondage->addReponse($reponse);
                $manager->persist($reponse);
            }
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [SondageFixtures::class];
    }
}
