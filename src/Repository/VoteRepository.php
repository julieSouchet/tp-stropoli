<?php

namespace App\Repository;

use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\User;
use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vote::class);
    }

    /**
     * Checks whether this particular User has already voted on this particular Sondage.
     * @param Sondage $sondage
     * @param User $user
     * @return bool
     */
    public function hasVoted(Sondage $sondage, User $user)
    {
        $votes = $this->findBy([
            'sondage' => $sondage,
            'voteur' => $user]);

        return count($votes) > 0;
    }

    /**
     * Checks whether this one vote ("user" has voted "reponse" for "sondage") exists or not.
     * @param User $user
     * @param Sondage $sondage
     * @param Reponse $reponse
     * @return bool
     */
    public function exists(User $user, Sondage $sondage, Reponse $reponse)
    {
        return !is_null($this->findOneBy([
            'voteur' => $user,
            'sondage' => $sondage,
            'reponse' => $reponse
        ]));
    }

    /**
     * Counts, and returns the number of votes for each Reponse of this Sondage in the form of an multi-dimensional array :
     *
     *  $results = [
     *      ['reponse' => Reponse, 'voteNb' => voteNumber],
     *      ... and so on for each Reponse
     *  ]
     * @param Sondage $sondage
     * @return Array
     */
    public function voteCount(Sondage $sondage) : Array
    {
        $queryResults = $this->createQueryBuilder('v')
            ->select('(v.reponse)')
            ->addSelect('count(v.voteur)')
            ->andWhere('v.sondage = :sondage')
            ->setParameter('sondage', $sondage->getId())
            ->addGroupBy('v.reponse')
            ->getQuery()
            ->execute();

        $results = [];
        $reponseRepo = $this->getEntityManager()->getRepository(Reponse::class);
        foreach ($queryResults as $result) {
            $reponse = $reponseRepo->find($result[1]);
            $voteNb = $result[2];
            $results[$result[1]] = ['reponse' => $reponse, 'voteNb' => $voteNb];
        }
        return $results;
    }

    // /**
    //  * @return Vote[] Returns an array of Vote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vote
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
