<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class UserController extends AbstractController
{
    /**
     * @Route("/user/", name="user_index", methods={"GET"})
     * @param UserRepository $userRepository
     * @return Response
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/user/new", name="user_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/profile/", name="user_sondages", methods={"GET"})
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function userSondages(Breadcrumbs $breadcrumbs)
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('profile.sondages.title', 'user_sondages');

        $user = $this->getUser();

        return $this->render('profile/sondages.html.twig', [
            'sondages' => $user->getSondages()
        ]);
    }

    /**
     * @Route("/profile/{id}", name="user_peek", methods={"GET"}, requirements={"id"="\d+"})
     * @param User $user
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function userPeek(User $user, Breadcrumbs $breadcrumbs)
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem(
            'profile.sondages.other',
            'user_peek',
            ['id'=>$user->getId()],
            RouterInterface::ABSOLUTE_PATH,
            ['username'=>$user->getUsername()]
        );

        return $this->render('profile/peek.html.twig', [
            'sondages' => $user->getSondages(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/votes", name="user_votes", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function userVotes(EntityManagerInterface $entityManager, Breadcrumbs $breadcrumbs)
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('profile.reponses.title', 'user_votes');

        // fetching the Sondages and Reponses that this User has chosen
        /*$reponseRepository = $entityManager->getRepository(Reponse::class);
        $sondageRepository = $entityManager->getRepository(Sondage::class);*/
        $user = $this->getUser();
        $votes = [];
        foreach ($user->getVotes() as $vote) {
            /*$sondage = $sondageRepository->find($vote->getSondage());
            $reponse = $reponseRepository->find($vote->getReponse());*/
            $votes[$vote->getSondage()->getQuestion()][] = ['vote'=>$vote, 'reponse'=>$vote->getReponse()->getTexte()];
        }
        return $this->render('profile/reponses.html.twig', [
           'votes' => $votes
        ]);
    }
}
