<?php

namespace App\Controller;

use App\Repository\SondageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search", methods={"GET"})
     * @param Breadcrumbs $breadcrumbs
     * @param SondageRepository $sondageRepository
     * @param Request $request
     * @return Response
     */
    public function index(Breadcrumbs $breadcrumbs, SondageRepository $sondageRepository, Request $request)
    {
        $breadcrumbs->addRouteItem('Accueil', 'main');
        $breadcrumbs->addRouteItem('Tous les sondages', 'sondage_index');
        $breadcrumbs->addRouteItem('Recherche', 'search');

        $query = $request->query->get('query');
        $sondages = $sondageRepository->findByQuestion($query);
        return $this->render('search/index.html.twig', [
            'sondages' => $sondages,
            'query' => $query
        ]);
    }
}
