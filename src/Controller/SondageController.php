<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Entity\User;
use App\Entity\Vote;
use App\Entity\Reponse;
use App\Form\SondageType;
use App\Form\SondageVoteType;
use App\Repository\SondageRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/sondage")
 */
class SondageController extends AbstractController
{
    /**
     * @Route("/", name="sondage_index", methods={"GET"})
     * @param SondageRepository $sondageRepository
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function index(SondageRepository $sondageRepository, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('sondage_all.title', 'sondage_index');

        return $this->render('sondage/index.html.twig', [
            'sondages' => $sondageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sondage_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function new(Request $request, UserRepository $userRepository, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('sondage_new.title', 'sondage_new');

        $sondage = new Sondage();
        // minimum 2 Reponses
        $sondage->addReponse(new Reponse());
        $sondage->addReponse(new Reponse());
        // form
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sondage->setDateCreation(new DateTime());
            $entityManager = $this->getDoctrine()->getManager();

            // createur
            $idCreateur = $request->request->get("createur");
            if ($idCreateur) {
                $sondage->setCreateur($userRepository->find($idCreateur));
            }

            $entityManager->persist($sondage);
            $entityManager->flush();

            return $this->redirectToRoute("sondage_show",["id"=>$sondage->getId()]);
        }

        return $this->render('sondage/new.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_show", methods={"GET"})
     * @param Sondage $sondage
     * @param EntityManagerInterface $entityManager
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function show(Sondage $sondage, EntityManagerInterface $entityManager, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('sondage_all.title', 'sondage_index');
        $breadcrumbs->addRouteItem('sondage_show.title', 'sondage_show', ['id' => $sondage->getId()], RouterInterface::ABSOLUTE_PATH, ['id' => $sondage->getId()]);

        $voteRepository = $entityManager->getRepository(Vote::class);
        return $this->render('sondage/show.html.twig', [
            'sondage' => $sondage,
            'votes' => $voteRepository->voteCount($sondage)
        ]);
    }

    /**
     * @Route("/{id}/vote", name="sondage_vote", methods={"GET", "POST"})
     * @param Sondage $sondage
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function vote(Sondage $sondage, Request $request, EntityManagerInterface $entityManager, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addRouteItem('accueil', 'main');
        $breadcrumbs->addRouteItem('sondage_all.title', 'sondage_index');
        $breadcrumbs->addRouteItem('sondage_show.title', 'sondage_show', ['id' => $sondage->getId()], RouterInterface::ABSOLUTE_PATH, ['id' => $sondage->getId()]);
        $breadcrumbs->addRouteItem('sondage_vote.title', 'sondage_vote', ['id' => $sondage->getId()], RouterInterface::ABSOLUTE_PATH, ['id' => $sondage->getId()]);

        $form = $this->createForm(SondageVoteType::class, $sondage, [
            'attr' => ['id' => 'vote-form']
        ]);
        $form->handleRequest($request);

        // getting the voting User
        $user = $this->getUser();

        if($form->isSubmitted() && $form->isValid()) {
            // reading the answers
            if ($sondage->getMultiple()) {
                $reponses = $form['votes']->getData();
            } else {
                $reponses = [$form['votes']->getData()];
            }

            // creating new Votes for each answer
            foreach ($reponses as $reponse) {
                $vote = new Vote();
                $vote->setReponse($reponse);
                $reponse->addVote($vote);
                $vote->setSondage($sondage);
                $sondage->addVote($vote);
                $vote->setVoteur($user);
                $user->addVote($vote); // it's fine
                $vote->setDate(new DateTime());
                $entityManager->persist($vote);
            }

            $entityManager->flush();
            return $this->redirectToRoute('sondage_show', ['id'=>$sondage->getId()]);
        }

        return $this->render('sondage/vote.html.twig', [
            'sondage' => $sondage,
            'myForm' => $form->createView(),
            'hasVoted' => $entityManager->getRepository(Vote::class)->hasVoted($sondage, $user)
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sondage_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Sondage $sondage
     * @return Response
     */
    public function edit(Request $request, Sondage $sondage): Response
    {
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sondage_index');
        }

        return $this->render('sondage/edit.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_delete", methods={"DELETE"})
     * @param Request $request
     * @param Sondage $sondage
     * @return Response
     */
    public function delete(Request $request, Sondage $sondage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sondage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sondage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sondage_index');
    }
}
