<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoteRepository::class)
 * @ORM\Table(name="vote", uniqueConstraints={@ORM\UniqueConstraint(name="unique_vote", columns={"sondage_id", "reponse_id", "voteur_id"})})
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Sondage::class, inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sondage;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voteur;

    /**
     * @ORM\ManyToOne(targetEntity=Reponse::class, inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reponse;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSondage(): ?Sondage
    {
        return $this->sondage;
    }

    public function setSondage(?Sondage $sondage): self
    {
        $this->sondage = $sondage;

        return $this;
    }

    public function getVoteur(): ?User
    {
        return $this->voteur;
    }

    public function setVoteur(?User $voteur): self
    {
        $this->voteur = $voteur;

        return $this;
    }

    public function getReponse(): ?Reponse
    {
        return $this->reponse;
    }

    public function setReponse(?Reponse $reponse): self
    {
        $this->reponse = $reponse;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
